package com.sergio.alvarez.mieconomia

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.sergio.alvarez.mieconomia.databinding.ActivityHowToBinding

class HowTo : AppCompatActivity() {

    private lateinit var vb: ActivityHowToBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vb = ActivityHowToBinding.inflate(layoutInflater)
        setContentView(vb.root)


    }
}